import React from 'react';

const Dropdown = ({ selected, options = [], name, handler }) => {
    return (
        <select name={name} onChange={handler}>
            {options.map((item) => {
                const isSelected = selected===item ? 'selected' : '';
                if (isSelected) {
                    return(
                        <option value={item} selected>{item}</option>
                    );
                } else {
                    return(
                        <option value={item} >{item}</option>
                    );
                }
            })}
        </select>
    );
};

export default Dropdown;
