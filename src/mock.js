export default {
    flList: [
        'fl0',
        'fl1',
        'fl2'
    ],
    sftList: [
        [
            'sft00',
            'sft01',
            'sft02',
            'sft03'
        ],
        [
            'sft10',
            'sft11',
            'sft12',
            'sft13'
        ],
        [
            'sft20',
            'sft21',
            'sft22',
            'sft23'
        ]
    ],
    selected: [
        {
            funcVal: 'fl0',
            shift: 'sft00'
        },
        {
            funcVal: 'fl0',
            shift: 'sft03'
        }
    ]
};
