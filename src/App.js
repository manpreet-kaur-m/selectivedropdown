import React, { Component } from 'react';
import './App.css';
import Dropdown from './Dropdown';
import data from './mock';

// console.log(data);

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      flList: data.flList,
      sftList: data.sftList,
      selected: data.selected
    }
  }

  handleChange(event) {
    const dropDownDetail = event.target.name.split('-');;
    const isLeft = dropDownDetail[0] === 'fl';
    const index = dropDownDetail[1];
    const value = event.target.value;
    const replacementItem = {
      funcVal: isLeft ? value : data.selected[index].funcVal,
      shift: !isLeft ? value : data.selected[index].shift
    }
    const newSelectedOptions = Object.assign([], data.selected, {[index]: replacementItem});
    console.log(replacementItem, newSelectedOptions, data.selected, this.state.selected);
    this.setState({
      ...this.state,
      selected: newSelectedOptions
    });
  }

  render() {
    const {
      flList,
      sftList,
      selected
    } = this.state;

    console.log(this.state);

    const removeOption = (item, selectedShift) => {
      let remove = false;
      for (let i = 0; i < selected.length; i += 1) {
        const { shift } = selected[i];
        if (shift === item && shift !== selectedShift) {
          remove = true;
          break;
        }
      }
      return remove;
    };

    return (
      <div className="App">
        <header className="App-header">
          {
            selected.map((item, index) => {
              const { funcVal, shift } = item;
              console.log(shift, index);
              const shiftIndex = flList.indexOf(funcVal);
              const shiftOptions = sftList[shiftIndex].reduce((accumulator, item) => {
                  if (!removeOption(item, shift)) {
                    accumulator.push(item);
                  }
                  return accumulator;
              }, []);
              return(
                <div className="row">
                  <Dropdown
                    selected={funcVal}
                    options={flList}
                    name={`fl-${index}`}
                    handler={this.handleChange}
                  />
                  <Dropdown
                    selected={shift}
                    options={shiftOptions}
                    name={`sft-${index}`}
                    handler={this.handleChange}
                  />
                </div>
              );
            }) 
          }
        </header>
      </div>
    );
  }
}

export default App;
